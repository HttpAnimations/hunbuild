#!/bin/bash

# Prompt for the new folder name
read -p "Enter the new folder name: " folder_name

# Create a new directory with the input name
mkdir -p "$folder_name"

# Path of the source directory
src_dir="REPLACEME"  # Replace REPLACEME with the path of the source directory

# Copy files from the source directory to the new directory
cp -a "$src_dir/." "$folder_name/"

# Change directory to the new folder
cd "$folder_name"

# Rename files and replace contents
for file in *; do
    # Extract the file extension
    extension="${file##*.}"
    # Rename the file to folder_name.extension
    mv "$file" "${folder_name}.${extension}"
    # Replace REPLACEME in file content with the new folder name
    sed -i "s/REPLACEME/$folder_name/g" "${folder_name}.${extension}"
done

echo "Files have been processed and moved to $folder_name."
