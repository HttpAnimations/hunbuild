async function fetchJSON(url) {
    try {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Failed to fetch JSON from ' + url, error);
        return null; // Return null in case of failure to handle it gracefully
    }
}

async function loadTorrents() {
    const repoInfo = await fetchJSON('Other.json'); // Adjust this URL if necessary
    if (!repoInfo || !repoInfo.url) {
        console.error('Invalid repository info');
        document.getElementById('contentArea').innerHTML = '<p>Error fetching repository information. Check console for details.</p>';
        return;
    }

    const torrentsData = await fetchJSON(repoInfo.url); // Fetching data from the URL provided in MRepo.json
    if (!torrentsData || !torrentsData.Torrents) {
        console.error('Invalid torrents data');
        document.getElementById('contentArea').innerHTML = '<p>Error fetching torrents data. Check console for details.</p>';
        return;
    }

    const contentArea = document.getElementById('contentArea');
    contentArea.innerHTML = '<h2>REPLACEME Torrents:</h2>'; // Clear initial content or add title

    torrentsData.Torrents.forEach(torrent => {
        const torrentDiv = document.createElement('div');
        torrentDiv.className = 'torrent';
        torrentDiv.innerHTML = `
        <h4>${torrent.Name}</h4>
        <p>Seeders: ${torrent.SeedersAtTheTime}</p>
        <p>Publisher: ${torrent.Publisher}</p>
        <div class="download-section">
            <p>Download:</p>
            <a href="${torrent.MagnetUrl}" class="button">Magnet</a>
            <a href="${torrent[".Torrent"]}" download="${torrent[".Torrent"].split('/').pop()}" class="button">Torrent</a>
        </div>
        <div class="source-section">
            <p>Source: <a href="${torrent.Source}" target="_blank">Open Page</a></p>
        </div>
        ${torrent.HasStreamURL ? `<p>Stream: <a href="${torrent.StreamURL}" target="_blank">Watch Now</a></p>` : ''}
    `;
        contentArea.appendChild(torrentDiv);
    });
}

loadTorrents();
